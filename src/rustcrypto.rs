use crypto::sha1::Sha1;
use crypto::digest::Digest;
use super::AppError;
use std::result::Result;
use std::path::PathBuf;
use std::fs::OpenOptions;
use std::io::Read;

pub struct RustCrypto;

impl RustCrypto {
    pub fn new(id: &str) -> Result<super::BoxedHasher, AppError> {
        if id == "sha1-128" {
            Ok(Box::new(SHA1Hasher {
                bufsize: 128 * 1024
            }))
        } else if id == "sha1-256" {
            Ok(Box::new(SHA1Hasher {
                bufsize: 256 * 1024
            }))
        } else {
            Err(AppError::UnsupportedHasher)
        }
    }
}

struct SHA1Hasher {
    bufsize: usize,
}

impl super::Hasher for SHA1Hasher {
    fn hexdigest(&self, path_buf: &PathBuf) -> Result<String, AppError> {
        let mut f = OpenOptions::new().read(true).open(path_buf)?;
        let mut file_size = f.metadata()?.len();
        let mut proc = Sha1::new();
        let mut buf = Vec::new();
        buf.resize(self.bufsize, 0);

        while file_size > 0 {
            let bytes_read = f.read(&mut buf)?;
            if bytes_read == 0 { return Err(AppError::HasherIOError); }
            proc.input(&buf[0..bytes_read]);
            file_size -= bytes_read as u64;
        }

        Ok(proc.result_str())
    }

    fn name(&self) -> &'static str { "RC-SHA1" }
}
