use clap::{Arg, App};
use std::io;
use std::path::PathBuf;

const ARG_FILES: &str = "files";
const ARG_RC_SHA1: &str = "rc-sha1";
const ARG_MIN_SHA1: &str = "min-sha1";
const ARG_RUST_SHA1: &str = "rust-sha1";

extern crate clap;
extern crate thiserror;
extern crate walkdir;
extern crate crypto;
extern crate sha1;
extern crate sha;
extern crate hexdump;
extern crate byteorder;

mod rustcrypto;
mod minsha1;
mod rustsha;

#[derive(thiserror::Error, Debug)]
pub enum AppError {
    #[error("I/O error")]
    IOError(#[from] io::Error),

    #[error("I/O error during file discovery")]
    WalkdirError(#[from] walkdir::Error),

    #[error("File not found")]
    FileNotFoundError(String),

    #[error("No hasher selected")]
    NoHasherSelected,

    #[error("Unsupported hashing algorithm requested")]
    UnsupportedHasher,

    #[error("Unspecified I/O error")]
    HasherIOError,

    #[error("Some errors encountered during runtime.")]
    SomeErrorsEncountered,
}

struct AppMode {
    use_rustcrypto_sha1: bool,
    use_min_sha1: bool,
    use_rust_sha1: bool,
}

pub trait Hasher {
    fn hexdigest(&self, path_buf: &PathBuf) -> Result<String, AppError>;
    fn name(&self) -> &'static str;
}

type BoxedHasher = Box<dyn Hasher>;
type HasherVec = Vec<BoxedHasher>;

fn process_path(path_buf: &PathBuf, hashers: &HasherVec) -> Result<(), AppError> {
    for hasher in hashers {
        let hexdigest = hasher.hexdigest(&path_buf)?;
        let path_str = path_buf.to_str().unwrap_or("Corrupted path");

        println!("{} {} {}", hasher.name(), hexdigest, path_str);
    }

    Ok(())
}

fn process_arg(f: &str, hashers: &HasherVec) -> Result<(), AppError> {
    for entry in walkdir::WalkDir::new(f) {
        let path = entry?.path().to_path_buf();
        let is_dir = std::fs::metadata(&path)?.is_dir();

        if is_dir {
            continue;
        }

        match process_path(&path, hashers) {
            Ok(_) => (),
            Err(msg) => println!("error: {} (file {})", msg, path.to_str().unwrap_or("?")),
        }
    }

    Ok(())
}

fn process(args: Vec<&str>, mode: &AppMode) -> Result<(), AppError> {
    let mut hashers = Vec::new();
    let mut errors = false;

    if mode.use_rustcrypto_sha1 {
        hashers.push(rustcrypto::RustCrypto::new("sha1-128")?);
    }

    if mode.use_min_sha1 {
        hashers.push(minsha1::MinSha1::new("sha1")?);
    }

    if mode.use_rust_sha1 {
        hashers.push(rustsha::RustSha::new("sha1")?);
    }

    if hashers.is_empty() { return Err(AppError::NoHasherSelected); }

    for arg in args {
        match process_arg(arg, &hashers) {
            Ok(_) => (),
            Err(msg) => {
                println!("Fatal error: {}", msg);
                errors = true;
            }
        }
    }

    if errors {
        Err(AppError::SomeErrorsEncountered)
    } else {
        Ok(())
    }
}

fn run(m: clap::ArgMatches) -> i32 {
    let files = match m.values_of(ARG_FILES) {
        Some(values) => values.collect::<Vec<_>>(),
        None => Vec::new()
    };

    let mode = AppMode {
        use_rustcrypto_sha1: m.is_present(ARG_RC_SHA1),
        use_min_sha1: m.is_present(ARG_MIN_SHA1),
        use_rust_sha1: m.is_present(ARG_RUST_SHA1),
    };

    match process(files, &mode) {
        Ok(_) => 0,
        Err(_) =>
            // This will only ignore errors created in `process`, not deeper,
            // because `process` has its own error handler that dumps errors.
            1
    }
}

fn main() {
    let matches = App::new(env!("CARGO_PKG_NAME"))
        .version(env!("CARGO_PKG_VERSION"))
        .about("Hash calculator using Rust implementations of hash functions")
        .arg(Arg::with_name(ARG_RC_SHA1).long("rc-sha1").about("Use SHA-1 from rust-crypto crate"))
        .arg(Arg::with_name(ARG_MIN_SHA1).long("min-sha1").about("Use SHA-1 from sha1 crate"))
        .arg(Arg::with_name(ARG_RUST_SHA1).long("rust-sha1").about("Use SHA-1 from sha crate"))
        .arg(Arg::with_name(ARG_FILES).multiple(true).about("Files to hash").required(true))
        .get_matches();

    std::process::exit(run(matches));
}
