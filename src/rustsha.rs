use super::AppError;
use std::io::Read;
use std::fs::OpenOptions;
use std::path::PathBuf;
use byteorder::{BigEndian, WriteBytesExt};

pub struct RustSha;

impl RustSha {
    pub fn new(id: &str) -> Result<super::BoxedHasher, AppError> {
        if id == "sha1" {
            Ok(Box::new(SHA1Hasher {}))
        } else {
            Err(AppError::UnsupportedHasher)
        }
    }
}

struct SHA1Hasher;

impl super::Hasher for SHA1Hasher {
    fn hexdigest(&self, path_buf: &PathBuf) -> Result<String, AppError> {
        let mut f = OpenOptions::new().read(true).open(path_buf)?;
        let mut file_size = f.metadata()?.len();
        let orig_file_size = file_size;
        let mut buf = Vec::new();
        buf.resize(64 * 1024, 0);

        let mut state = sha::sha1::consts::H;
        let mut lastchunk = Vec::<u8>::new();

        'outer: while file_size > 0 {
            let bytes_read = f.read(&mut buf)?;
            if bytes_read == 0 { return Err(AppError::HasherIOError); }

            for block in buf[0..bytes_read].chunks(64) {
                if block.len() < 64 {
                    lastchunk = block.to_vec();
                    break 'outer;
                }

                sha::sha1::ops::digest_block(&mut state, &block);
            }

            file_size -= bytes_read as u64;
        }

        // SHA-1 padding from RFC-3174:
        //
        // "As a summary, a "1" followed by m "0"s followed by a 64- bit integer are appended to
        // the end of the message to produce a padded message of length 512 * n.  The 64-bit
        // integer is the length of the original message.  The padded message is then processed by
        // the SHA-1 as n 512-bit blocks."
        let _ = lastchunk.write_u8(0x80)?;

        if lastchunk.len() + 9 > 65 {
            for _ in 0..(64 + 65 - lastchunk.len() - 8) {
                let _ = lastchunk.write_u8(0x0)?;
            }

            let _ = lastchunk.write_u64::<BigEndian>(8 * orig_file_size)?;
            sha::sha1::ops::digest_block(&mut state, &lastchunk[0..64]);
            sha::sha1::ops::digest_block(&mut state, &lastchunk[65..]);
        } else {
            for _ in 0..(65 - lastchunk.len() - 9) {
                let _ = lastchunk.write_u8(0x0)?;
            }

            let _ = lastchunk.write_u64::<BigEndian>(8 * orig_file_size)?;
            sha::sha1::ops::digest_block(&mut state, &lastchunk);
        }

        let hash = format!("{:08x}{:08x}{:08x}{:08x}{:08x}",
            state[0], state[1], state[2],
            state[3], state[4]);

        Ok(hash)
    }

    fn name(&self) -> &'static str { "RUST-SHA1" }
}

