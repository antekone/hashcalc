use super::AppError;
use std::io::Read;
use std::fs::OpenOptions;
use std::path::PathBuf;

pub struct MinSha1;

impl MinSha1 {
    pub fn new(id: &str) -> Result<super::BoxedHasher, AppError> {
        if id == "sha1" {
            Ok(Box::new(SHA1Hasher {}))
        } else {
            Err(AppError::UnsupportedHasher)
        }
    }
}

struct SHA1Hasher;

impl super::Hasher for SHA1Hasher {
    fn hexdigest(&self, path_buf: &PathBuf) -> Result<String, AppError> {
        let mut f = OpenOptions::new().read(true).open(path_buf)?;
        let mut file_size = f.metadata()?.len();
        let mut proc = sha1::Sha1::new();
        let mut buf = Vec::new();
        buf.resize(128 * 1024, 0);

        while file_size > 0 {
            let bytes_read = f.read(&mut buf)?;
            if bytes_read == 0 { return Err(AppError::HasherIOError); }
            proc.update(&buf[0..bytes_read]);
            file_size -= bytes_read as u64;
        }

        Ok(proc.digest().to_string())
    }

    fn name(&self) -> &'static str { "MIN-SHA1" }
}
