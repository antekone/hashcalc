#!/usr/bin/env sh

build() {
    RUSTFLAGS="$RUSTFLAGS -A dead_code -A unused_variables -A unused_imports -A unused_mut" cargo build
}

deploy() {
    true
}

build
deploy

echo Press ENTER key to continue...
read x
